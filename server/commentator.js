'use strict';

const Factory = require('./factory');

const getMilliSecond = second => second * 1000; // pure function

class Commentator { // commentator is a facade
  constructor(name, room, io) {
    this.name = name;
    this.room = room;
    this.io = io;
    this.mind = new Factory();

    this.toIntroduceRacers = new Proxy(this._toIntroduceRacers, {  // using Proxy pattern and higher-order functions
      apply: (target, context) => {
        setTimeout(() => {
          target.apply(context);
          this.toSayFact();
        }, getMilliSecond(5));
        return context; // return this for chaining
      }
    });

    this.toShowStateNow = new Proxy(this._toShowStateNow, {
      apply: (target, context) => {
        this.interval = setInterval(() => {
          target.apply(context);
          this.toSayJoke();
        }, getMilliSecond(30));
        return context;
      }
    });

    this.toShowResults = new Proxy(this._toShowResults, {
      apply: (target, context) => {
        target.apply(context);
        clearInterval(this.interval);
        return context;
      }
    });

    this.toSayJoke = new Proxy(this._toSayJoke, {
      apply: (target, context) => {
        setTimeout(() => {
          target.apply(context);
        }, getMilliSecond(15));
        return context;
      }
    });

    this.toSayFact = new Proxy(this._toSayFact, {
      apply: (target, context) => {
        setTimeout(() => {
          target.apply(context);
        }, getMilliSecond(10));
        return context;
      }
    });
  }

  toIntroduceSelf() {
    const message = `Доброго времени суток, дорогие любители клавогонок!
    С Вами как всегда я - ${this.name}. 
    И сегодня мы будем наблюдать за увлекательнейшней гонкой!`;
    this.io.to(this.room.name).emit('message', message);
    return this; // return this for chaining
  }

  _toIntroduceRacers() {
    let message = 'Что ж, пора представить наших участников!';
    this.room.players.forEach((player, index) => {
      message += `<br>Под номером ${index + 1} едет ${player}`;
    });
    this.io.to(this.room.name).emit('message', message);
  }

  _toShowStateNow() {
    let message = 'Что же у нас есть на данный момент:';
    this.room.leaderBoard.forEach((player, index) => {
      message += `<br>${index + 1}. ${player.login} и он 
          ввел уже ${player.count} символов`;
    });
    message += `<br> А до конца гонки у нас 
        остается ${this.room.timerInGame} секунд`;
    this.io.to(this.room.name).emit('message', message);
  }

  toShowCloseFinish(name) {
    const message = `Итак, гонщик под ником ${name} 
    выходит на финишную прямую!`;
    this.io.to(this.room.name).emit('message', message);
    return this;
  }

  toShowFinishedRacer(name) {
    const message = `Поздравляем, гонщик под ником ${name} 
    достиг финиша!`;
    this.io.to(this.room.name).emit('message', message);
    return this;
  }

  _toShowResults() {
    const first = this.room.leaderBoard[0];
    const second = this.room.leaderBoard[1];
    const third = this.room.leaderBoard[2];
    const message = this._getThreeFirst(first)(second)(third); // using currying
    this.io.to(this.room.name).emit('message', message);
  }

  _toSayJoke() {
    const message = this.mind.generate('joke');
    this.io.to(this.room.name).emit('message', message.data);
  }

  _toSayFact() {
    const message = this.mind.generate('fact');
    this.io.to(this.room.name).emit('message', message.data);
  }

  _getThreeFirst(first) {
    let message = `Вот и закончилась наша гонка! И вот что мы имеем:<br>
    Первое место занимает ${first.login} (ввел ${first.count} символов) 
    и потратил на заезд ${first.time} секунд.<br>`;
    return second => {
      if (second) {
        message += `На втором месте расположился ${second.login} (ввел ${second.count} символов) 
            с результатом в ${second.time} секунд<br>`;
        return third => {
          if (third) {
            message += `И на третьем месте ${third.login} с ${third.time} секундами (ввел ${third.count} символов)<br>
                Спасибо вам, дорогие зрители за внимание! 
                Для вас комментировал этот заезд ${this.name}. До скорых встреч!`;
            return message;
          } else {
            message += `Спасибо вам, дорогие зрители за внимание! 
                        Для вас комментировал этот заезд ${this.name}. До скорых встреч!`;
            return message;
          }
        };
      } else {
        message += `Спасибо вам, дорогие зрители за внимание! 
            Для вас комментировал этот заезд ${this.name}. До скорых встреч!`;
        return () => message;
      }
    };
  }
}

module.exports = Commentator;

