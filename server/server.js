'use strict';

const path = require('path');
const express = require('express');
const app = express();
const server = require('http').Server(app);
const io = require('socket.io')(server);
const jwt = require('jsonwebtoken');
const passport = require('passport');
const bodyParser = require('body-parser');

const users = require('./users.json');
const texts = require('./texts.json');

const Commentator = require('./commentator');

require('./passport.config');

server.listen(3000);

app.use(express.static(path.join(__dirname, '../client/public')));
app.use(passport.initialize());
app.use(bodyParser.json());

app.get('/', (req, res) => {
  res.sendFile(path.join(__dirname, '../client/index.html'));
});

app.get('/login', (req, res) => {
  res.sendFile(path.join(__dirname, '../client/login.html'));
});

app.get('/race', (req, res) => {
  res.sendFile(path.join(__dirname, '../client/race.html'));
});

app.get(
  '/text',
  passport.authenticate('jwt', { session: false }),
  (req, res) => {
    res.status(200).json({ texts });
  }
);

app.post('/login', (req, res) => {
  const userFromReq = req.body;
  const userInDB = users.find(user => user.email === userFromReq.email);
  if (userInDB && userInDB.password === userFromReq.password) {
    const token = jwt.sign(userInDB, 'secret');
    res.status(200).json({ auth: true, token });
  } else {
    res.status(401).json({ auth: false });
  }
});

const rooms = new Array(10).fill(0).map((room, index) => ({
  name: `/room_${index}`,
  sockets: [],
  gameStarted: false,
  timerBeforeGame: 10,
  timerInGame: 200,
  timerAfterGame: 20,
  players: [],
  finished: 0
}));

io.on('connection', socket => {
  const room = rooms[getRoomNumber()];
  const commentator = new Commentator('Александр Александрович', room, io);
  room.sockets.push(socket);

  socket.join(room.name);
  socket.emit('room');

  socket.on('close', payload => {
    const { token, login } = payload;
    const user = jwt.verify(token, 'secret');
    if (user) {
      io.to(room.name).emit('leave', { login });
      room.players = room.players.filter(player => login !== player);
    }
  });

  socket.on('sockedConnected', token => {
    const user = jwt.verify(token, 'secret');
    if (user) {
      const userLogin = jwt.decode(token).login;

      io.to(room.name).emit('newPlayer', { login: userLogin });
      socket.emit('existPlayers', { players: room.players });

      room.players.push(userLogin);
    }
  });

  socket.on('keydown', payload => {
    const { token, login, counterSymbols, time, maxCount } = payload;
    const user = jwt.verify(token, 'secret');
    if (user) {
      io.to(room.name).emit('progress', {
        login,
        counterSymbols: counterSymbols < 0 ? 0 : counterSymbols,
        time,
      });
      if ((maxCount - counterSymbols) === 30)
        commentator.toShowCloseFinish(login)
          .toSayFact();
    }
  });

  socket.on('leaderBoard', leaderBoard => {
    room.leaderBoard = leaderBoard;
  });

  socket.on('finish', token => {
    const user = jwt.verify(token, 'secret');
    if (user) {
      commentator.toShowFinishedRacer(user.login)
        .toSayFact();
      room.finished++;
      if (room.finished === room.players.length) {
        room.timerInGame = 0;
        room.finished = 0;
      }
    }
  });

  if (room.sockets.length === 1) {
    room.timerBeforeGame = 10;
    room.timer = startBeforeTimer(room, commentator);
  }
});

function startGameTimer(room, commentator) {
  if (room.players.length > 0) {
    room.timerInGame = 200;
    const timerInGame = setInterval(() => {
      io.to(room.name).emit('counterInGame', room.timerInGame);
      if (room.timerInGame === 0) {
        io.to(room.name).emit('endGame');
        startAfterTimer(room, commentator);
        commentator.toShowResults();
        room.gameStarted = false;
        clearInterval(timerInGame);
      }
      room.timerInGame--;
    }, 1000);
    room.timer = timerInGame;
  }
}

function startAfterTimer(room, commentator) {
  room.timerAfterGame = 20;
  const timerAfterGame = setInterval(() => {
    io.to(room.name).emit('counterAfterGame', room.timerAfterGame);
    if (room.timerAfterGame === 0) {
      io.to(room.name).emit('startGame', { indexOfText: getRandom(0, 4) });
      commentator.toIntroduceSelf()
        .toIntroduceRacers()
        .toShowStateNow(); // using chaining
      startGameTimer(room, commentator);
      room.gameStarted = true;
      clearInterval(timerAfterGame);
    }
    room.timerAfterGame--;
  }, 1000);
}

function getRandom(min, max) {
  return Math.floor(Math.random() * (max - min + 1)) + min;
}

function getRoomNumber() {
  for (let i = 0; i < rooms.length; i++) {
    if (rooms[i].sockets.length < 4 && !rooms[i].gameStarted) return i;
  }
}

function startBeforeTimer(room, commentator) {
  const timerBeforeGame = setInterval(() => {
    io.to(room.name).emit('counterBeforeGame', room.timerBeforeGame);
    if (room.timerBeforeGame === 0) {
      io.to(room.name).emit('startGame', { indexOfText: getRandom(0, 4) });
      commentator.toIntroduceSelf()
        .toIntroduceRacers()
        .toShowStateNow();
      room.gameStarted = true;
      startGameTimer(room, commentator);
      clearInterval(timerBeforeGame);
    }
    room.timerBeforeGame--;
  }, 1000);
}
