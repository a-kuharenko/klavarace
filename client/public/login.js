'use strict';

window.onload = () => {
  const loginBtn = document.querySelector('#login-btn');
  const emailInput = document.querySelector('#email-input');
  const passwordInput = document.querySelector('#password-input');

  loginBtn.addEventListener('click', () => {
    fetch('/login', {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json'
      },
      body: JSON.stringify({
        email: emailInput.value,
        password: passwordInput.value
      })
    }).then(res => {
      res.json().then(body => {
        if (body.auth) {
          localStorage.setItem('jwt', body.token);
          location.replace('/race');
        } else {
          console.log('auth failed');
        }
      });
    }).catch(err => {
      console.log('request is wrong');
    });
  });
};
