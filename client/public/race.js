'use strict';
window.onload = () => {
  const jwt = localStorage.getItem('jwt');
  if (!jwt) {
    location.replace('/login');
  } else {
    const socket = io.connect('http://localhost:3000');

    const divRacers = document.querySelector('#racers');
    const textPlace = document.querySelector('#text');
    const spanTimeBeforeGame = document.querySelector('#timeBFG');
    const spanTimeAfterGame = document.querySelector('#timeAfterGame');
    const spanTimeInGame = document.querySelector('#timeInGame');
    const messageBox = document.querySelector('#messageBox');
    const body = document.querySelector('body');

    let textArray = [];
    let counterSymbols = -1;
    let time = 0;
    let timer = null;
    let login = '';
    let gameStarted = false;
    let playersList = [];

    window.onbeforeunload = () => {
      socket.emit('close', {
        login,
        token: jwt
      });
    };

    body.addEventListener('keydown', event => {
      if (gameStarted) {
        if (event.key === textArray[counterSymbols]) {
          textArray[counterSymbols] = `<span class="past">${event.key}</span>`;
          highlightNextSymbol(textArray, counterSymbols, textPlace);
          counterSymbols++;
          socket.emit('keydown', {
            login,
            counterSymbols,
            time,
            token: jwt,
            maxCount: textArray.length
          });
        }
      }
    });

    socket.on('room', () => {
      socket.emit('sockedConnected', jwt);
    });

    socket.on('message', message => {
      messageBox.innerHTML = message;
    });

    socket.on('leave', payload => {
      const { login } = payload;
      const progressBar = document.querySelector(`#${payload.login}`);
      const leaveText = document.createElement('span');

      leaveText.innerHTML = ' leaved';
      progressBar.style.display = 'none';
      progressBar.parentNode.appendChild(leaveText);

      playersList = playersList.filter(player => player !== login);
    });

    socket.on('progress', payload => {
      const { login: loginOfProgress, counterSymbols: symbols, time } = payload;

      const progressBar = document.querySelector(`#${loginOfProgress}`);
      progressBar.value = symbols;
      progressBar.max = textArray.length;

      if (
        symbols === textArray.length &&
        textArray.length !== 0 &&
        loginOfProgress === login
      ) {
        socket.emit('finish', jwt);
      }

      playersList.forEach(player => {
        const { login, div } = player;
        if (login === loginOfProgress) {
          player.count = symbols;
          player.time = time;
        }
        div.parentNode.removeChild(div);
      });

      playersList.sort((a, b) => {
        if (a.count === b.count) {
          if (a.time > b.time) return 1;
          if (a.time <= b.time) return -1;
        }
        if (a.count >= b.count) return -1;
        if (a.count <= b.count) return 1;
      });

      socket.emit('leaderBoard', playersList);

      playersList.forEach(player => {
        divRacers.appendChild(player.div);
      });
    });

    socket.on('newPlayer', payload => {
      if (!login) login = payload.login;
      createDivPlayer(payload.login, playersList, divRacers, textArray);
    });

    socket.on('existPlayers', payload => {
      const { players } = payload;
      players.forEach(player => {
        createDivPlayer(player, playersList, divRacers, textArray);
      });
    });

    socket.on('counterBeforeGame', count => {
      document.querySelector('.timerBefore').style.display = 'block';
      textPlace.style.display = 'none';
      divRacers.style.display = 'none';
      spanTimeBeforeGame.innerHTML = count + 's';
    });

    socket.on('counterInGame', count => {
      document.querySelector('.timerAfter').style.display = 'none';
      document.querySelector('.timerBefore').style.display = 'none';
      document.querySelector('.timerIn').style.display = 'block';
      spanTimeInGame.innerHTML = count + 's';
    });

    socket.on('counterAfterGame', count => {
      textPlace.style.display = 'none';
      gameStarted = false;
      document.querySelector('.timerIn').style.display = 'none';
      document.querySelector('.timerBefore').style.display = 'none';
      document.querySelector('.timerAfter').style.display = 'block';
      spanTimeAfterGame.innerHTML = count + 's';
    });

    socket.on('startGame', payload => {
      const index = payload.indexOfText;
      fetch('/text', {
        method: 'get',
        headers: {
          'Content-Type': 'application/json',
          Authorization: 'bearer ' + jwt
        }
      }).then(res => {
        res.json().then(body => {
          textArray = body.texts[index].split('');
          highlightNextSymbol(textArray, counterSymbols, textPlace);
          counterSymbols++;
        });
      });

      textPlace.style.display = 'block';
      divRacers.style.display = 'block';

      socket.emit('keydown', {
        login,
        counterSymbols,
        time,
        token: jwt
      });

      gameStarted = true;

      timer = setInterval(() => {
        console.log(time);
        time++;
      }, 1000);
    });

    socket.on('endGame', () => {
      textArray = [];
      counterSymbols = -1;
      time = 0;
      clearTimeout(timer);
      timer = null;
      playersList.forEach(player => {
        player.count = 0;
      });
    });
  }
};

function highlightNextSymbol(textArray, counterSymbols, textPlace) {
  const nextSymbol = textArray[counterSymbols + 1];
  if (nextSymbol) {
    textArray[counterSymbols + 1] = `<span class="next">${nextSymbol}</span>`;
    textPlace.innerHTML = textArray.join('');
    textArray[counterSymbols + 1] = nextSymbol;
  }
}

function createDivPlayer(login, players, divRacers) {
  const newSpan = document.createElement('span');
  const newProgresBar = document.createElement('progress');
  const newDiv = document.createElement('div');
  players.push({
    count: 0,
    div: newDiv,
    login
  });
  newProgresBar.value = 0;
  newProgresBar.id = login;

  newSpan.innerHTML = `${login}`;

  newDiv.appendChild(newSpan);
  newDiv.appendChild(newProgresBar);
  divRacers.appendChild(newDiv);
}

